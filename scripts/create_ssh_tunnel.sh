#!/bin/bash

k8sIP=$(kubectl describe svc/wordpress | grep IP: | awk '{print $2;}')
sudo ssh -L *:80:$k8sIP:80 10.0.0.50 -f -N
sudo ssh -L *:443:$k8sIP:443 10.0.0.50 -f -N 
